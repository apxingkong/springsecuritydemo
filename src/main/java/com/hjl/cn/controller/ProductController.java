package com.hjl.cn.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jialin
 * @create 2020-10-17
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {

    //http://localhost:18081/product/findAll
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/findAll")
    public String getAllProduct(){
        return "product-list";
    }

    @Secured("ROLE_USER")
    //http://localhost:18081/product/getOrderList
    @RequestMapping(value = "/getOrderList")
    public String getOrderList(){
        return "order-list";
    }

    @Secured({"ROLE_USER","ROLE_ADMIN"})
    //http://localhost:18081/product/getRoleList
    @RequestMapping(value = "/getRoleList")
    public String getRoleList(){
        return "permission-list";
    }

}
