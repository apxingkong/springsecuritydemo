package com.hjl.cn.mapper;

import com.hjl.cn.domain.SysRole;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @author jialin
 * @create 2020-10-17
 */
public interface RoleMapper extends Mapper<SysRole> {

    @Select("SELECT r.ID, r.ROLE_NAME roleName, r.ROLE_DESC roleDesc " +
            "FROM sys_role r, sys_user_role ur " +
            "WHERE r.ID=ur.RID AND ur.UID=#{uid}")
    public List<SysRole> findByUid(Integer uid);
}