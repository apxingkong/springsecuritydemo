package com.hjl.cn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@MapperScan("com.hjl.cn.mapper")
@EnableGlobalMethodSecurity(securedEnabled=true)
public class SpringsecuApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringsecuApplication.class, args);
    }

}
